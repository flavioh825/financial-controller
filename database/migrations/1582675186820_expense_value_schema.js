'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExpenseValueSchema extends Schema {
  up () {
    this.create('expense_values', (table) => {
      table.increments()
      table.decimal('value', 11, 2)
      table.datetime('was_paid')
      table.integer('expense_id')
          .unsigned().references('id').inTable('expenses')
      table.timestamps()
    })
  }

  down () {
    this.drop('expense_values')
  }
}

module.exports = ExpenseValueSchema
