'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BankAccountsSchema extends Schema {
  up () {
    this.create('bank_accounts', (table) => {
      table.increments()
      table.string('name', 50).notNullable()
      table.string('agency')
      table.string('account')
      table.integer('user_id')
          .unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('bank_accounts')
  }
}

module.exports = BankAccountsSchema
