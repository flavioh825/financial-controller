'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCreditCardFkIntoExpensesSchema extends Schema {
  up () {
    this.alter('expenses', (table) => {
      table.integer('credit_card_id')
        .unsigned().references('id').inTable('credit_cards').nullable();
    })
  }
}

module.exports = AddCreditCardFkIntoExpensesSchema
