'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExpensesSchema extends Schema {
  up () {
    this.create('expenses', (table) => {
      table.increments()
      table.string('description', 100).notNullable()
      table.string('form_payment', 50).notNullable()
      table.integer('installments_amount')
      table.decimal('installments_value', 11, 2)
      table.boolean('fixed_expense')
      table.integer('user_id')
          .unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('expendings')
  }
}

module.exports = ExpensesSchema
