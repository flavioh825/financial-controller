'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddExpirateDatesIntoCreditCardsSchema extends Schema {
  up () {
    this.table('credit_cards', (table) => {
      table.string('due_date', 2)
      table.string('expiration_date', 7)
      table.string('turn_on', 2)
    })
  }
}

module.exports = AddExpirateDatesIntoCreditCardsSchema
