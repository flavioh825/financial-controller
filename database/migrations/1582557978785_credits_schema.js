'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreditsSchema extends Schema {
  up () {
    this.create('credits', (table) => {
      table.increments()
      table.string('type', 100).notNullable()
      table.decimal('value', 11, 2).notNullable()
      table.integer('bank_account_id')
          .unsigned().references('id').inTable('bank_accounts')
      table.integer('user_id')
          .unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('credits')
  }
}

module.exports = CreditsSchema
