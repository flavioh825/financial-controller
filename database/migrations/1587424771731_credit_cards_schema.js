'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreditCardsSchema extends Schema {
  up () {
    this.create('credit_cards', (table) => {
      table.increments();
      table.string('name', 100).notNullable();
      table.string('last_four_numbers');
      table.string('flag_name').notNullable();
      table.decimal('limit', 11, 2);
      table.integer('user_id')
          .unsigned().references('id').inTable('users');
      table.timestamps();
    })
  }

  down () {
    this.drop('credit_cards')
  }
}

module.exports = CreditCardsSchema
