'use strict'

const ExpenseValue = use('App/Models/ExpenseValue');
const Expense = use('App/Models/Expense');

class ExpenseValueController {

  async create({ request }) {
    const params = request.only(['value', 'was_paid', 'expense_id']);
    const expense = await Expense.findOrFail(params.expense_id);
    const result = await expense.expenseValues().create(params);
    return result;
  }
}

module.exports = ExpenseValueController
