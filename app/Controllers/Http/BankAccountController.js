'use strict'

const BankAccount = use('App/Models/BankAccount');

class BankAccountController {

  async index() {
    const bankAccounts = BankAccount.all();
    return bankAccounts;
  }

  async store({ auth, request, response }) {
    const { id } = auth.user;
    const data = request.only([
      'name',
      'agency',
      'account',
    ]);
    const bankAccount = BankAccount.create({...data, user_id: id});
    return bankAccount;
  }

  async show({ params }) {
    const bankAccount = await BankAccount.findOrFail(params.id);
    return bankAccounts;
  }

  async update ({ params, request, response }) {
    const bankAccount = await BankAccount.findOrFail(params.id);
    const data = request.only([
      'name',
      'agency',
      'account',
      'user_id'
    ]);
    bankAccount.merge(data);
    await bankAccount.save();
    return bankAccount;
  }

  async destroy({ params, auth, response }) {
    const bankAccount = await BankAccount.findOrFail(params.id);
    if (bankAccount.user_id !== auth.user.id) {
      return response.status(401).send({ error: 'Not authorized' })
    }
    await bankAccount.delete();
  }

}

module.exports = BankAccountController
