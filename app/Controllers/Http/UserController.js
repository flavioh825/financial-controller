'use strict'

const User = use('App/Models/User');

class UserController {

  async create({ request }) {
    const data = request.only(['username', 'email', 'password']);
    const user = await User.create(data);
    return user;
  }

  async index() {
    const users = await User.all();
    return users;
  }

  async getUserData({ auth }) {
    const { id } = auth.user;
    const user = await User.query().with('bankAccounts').where('id', id).first();
    return user;
  }

}

module.exports = UserController
