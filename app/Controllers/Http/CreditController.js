'use strict'

const { format } = require('date-fns');

const Credit = use('App/Models/Credit');

class CreditController {

  async index({ request }) {
    const params = request.all();
    const credits = await Credit.query().queryCredits(params).fetch();
    return credits;
  }

  async store({ auth, request, response }) {
    const { id } = auth.user;
    const data = request.only([
      'type',
      'value',
      'bank_account_id',
      'created_at'
    ]);
    const credit = Credit.create({...data, user_id: id});
    return credit;
  }

  async show({ params }) {
    const credit = await Credit.findOrFail(params.id);
    return credit;
  }

  async update ({ params, request, response }) {
    const credit = await Credit.findOrFail(params.id);
    const data = request.only([
      'type',
      'value',
      'bank_account_id'
    ]);
    credit.merge(data);
    await credit.save();
    return credit;
  }

  async destroy({ params, auth, response }) {
    const credit = await Credit.findOrFail(params.id);
    if (credit.user_id !== auth.user.id) {
      return response.status(401).send({ error: 'Not authorized' })
    }
    await credit.delete();
  }

}

module.exports = CreditController
