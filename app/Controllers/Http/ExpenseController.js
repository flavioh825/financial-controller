'use strict'

const { format, addMonths, getYear, getMonth } = require('date-fns');
const Database = use('Database');
const { currentDateTime } = require('../../Helpers/AppHelper');
const Expense = use('App/Models/Expense');
const ExpenseValue = use('App/Models/ExpenseValue');

class ExpenseController {

  async index({ request }) {
    const params = request.all();
    const expenses = await Expense.queryExpenses(params);
    return expenses;
  }

  async store({ auth, request, response }) {
    const { id } = auth.user;
    const expenseValueParams = request.only(['expense_value']).expense_value;
    const expenseParams = request.only([
      'description', 'form_payment', 'installments_amount', 'installments_value', 
      'fixed_expense', 'credit_card_id', 'created_at' ]);

    let wasPaid = !expenseValueParams.was_paid ? null : format(new Date(currentDateTime()), 'yyyy-MM-dd HH:mm');
    let dateCreatedAt = new Date(expenseParams.created_at);

    if(expenseParams.installments_amount === null) {
      const expense = await Expense.create({ ...expenseParams, user_id: id });
      const expenseValue = await expense.expenseValues()
          .create({ ...expenseValueParams, was_paid: wasPaid });
    } else {
      const card = await Database.from('credit_cards').where('id', expenseParams.credit_card_id).first();
      let turn_on = new Date(getYear(dateCreatedAt), getMonth(dateCreatedAt), card.turn_on);
      let createdAt = new Date(getYear(dateCreatedAt), getMonth(dateCreatedAt), 1);

      for(let i=0; i < expenseParams.installments_amount; i++) {
        if(dateCreatedAt >= turn_on && i === 0) {
          createdAt = format(addMonths(firstDayMonth, i + 1), 'yyyy-MM-dd HH:mm:ss');
        } else {
          (i===0) ? (createdAt = format(new Date(dateCreatedAt), 'yyyy-MM-dd HH:mm:ss')) :
          (createdAt = format(addMonths(new Date(getYear(new Date(createdAt)), getMonth(new Date(createdAt)), 1), 1), 'yyyy-MM-dd HH:mm:ss'));
        }

        wasPaid = (wasPaid !== null && new Date(createdAt) <= new Date()) ? createdAt : null;

        const expense = await Expense.create({ ...expenseParams, user_id: id, created_at: createdAt });
        const expenseValue = await expense.expenseValues()
            .create({ ...expenseValueParams, was_paid: wasPaid, created_at: createdAt });
      }
    }

    const result = await ExpenseValue.query().with('expense').where('id', expenseValue.id).first();
    return result;
  }

  async show({ params }) {
    const expense = await Expense.getExpense(params.id);
    return expense;
  }

  async update({ params, request, response }) {
    const expense = Expense.findOrFail(params.id);
    const data = request.only([
      'description',
      'value',
      'form_payment',
      'installments_amount',
      'installments_value',
      'fixed_expense'
    ]);
    expense.merge(data);
    await expense.save();
    return expense;
  }

  async destroy({ params, auth, response }) {
    const expense = await findOrFail(params.id);
    if (expense.user_id !== auth.user.id) {
      return response.status(401).send({ error: 'Not authorized' })
    }
    await expense.delete();
  }
}

module.exports = ExpenseController
