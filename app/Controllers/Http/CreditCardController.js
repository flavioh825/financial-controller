'use strict'

const Database = use('Database');

const User = use('App/Models/User');
const CreditCard = use('App/Models/CreditCard');

class CreditCardController {

  async index({ auth, request }) {
    const { id } = auth.user;
    const user = await User.findOrFail(id);
    const creditCards = await user.creditCards().fetch();
    return creditCards;
  }

  store({auth, request}) {
    const { id } = auth.user;
    const data = request.only([
      'name',
      'last_four_numbers',
      'flag_name',
      'limit'
    ]);
    const creditCard = CreditCard.create({...data, user_id: id});
    return creditCard;
  }

}

module.exports = CreditCardController
