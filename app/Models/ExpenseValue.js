'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ExpenseValue extends Model {

  static get columns() {
    return [
      'id', 
      'expense_id',
      'value', 
      'was_paid', 
      'created_at', 
      'updated_at'
    ];
  }

  static get createdAtColumn () {
    return null;
  }

  expense() {
    return this.belongsTo('App/Models/Expense');
  }
}

module.exports = ExpenseValue
