'use strict'

const { format, lastDayOfMonth } = require('date-fns');
const { firstDayOfMonth } = require('../Helpers/AppHelper');

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Credit extends Model {

  static get columns() {
    return [
      'id', 
      'description', 
      'fixed_expense', 
      'form_payment', 
      'installments_amount',
      'installments_value',
      'created_at',
      'updated_at'
    ];
  }

  static get createdAtColumn () {
    return null;
  }

  bankAccount() {
    return this.belongsTo('App/Models/BankAccount');
  }

  user() {
    return this.belongsTo('App/Models/User');
  }

  static scopeQueryCredits(query, { ...params }) {
    const { user_id, all } = params;
    let { initialDate, finalDate } = params;
    if(!initialDate && !finalDate) {
      initialDate = firstDayOfMonth();
      finalDate = format(lastDayOfMonth(new Date()), 'yyyy-MM-dd');
    } else {
      initialDate = format(new Date(initialDate), 'yyyy-MM-dd');
      finalDate = format(new Date(finalDate), 'yyyy-MM-dd');
    }
    const sql = query.with('bankAccount')
                    .with('user')
                    .where('credits.created_at', '>=', `${initialDate} 00:00:00`)
                    .where('credits.created_at', '<=', `${finalDate} 23:59:59`);

    if(user_id && !all) return sql.where('credits.user_id', user_id);
    
    return sql.orderBy('credits.id','desc');
  }

}

module.exports = Credit
