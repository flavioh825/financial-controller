'use strict'

const { format, lastDayOfMonth, addDays } = require('date-fns');
const { firstDayOfMonth } = require('../Helpers/AppHelper');
const Database = use('Database')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const ExpenseValue = use('App/Models/ExpenseValue');
const User = use('App/Models/User');

class Expense extends Model {

  static get columns() {
    return [
      'id', 
      'description', 
      'fixed_expense', 
      'form_payment', 
      'installments_amount',
      'installments_value',
      'user_id',
      'credit_card_id',
      'created_at',
      'updated_at'
    ];
  }

  static get createdAtColumn () {
    return null;
  }

  user() {
    return this.belongsTo('App/Models/User');
  }

  expenseValues() {
    return this.hasMany('App/Models/ExpenseValue');
  }

  static async getExpense(id) {
    const expense = await Database.from('expenses').where('id', id).first();
    const expenseValue = await Database.from('expense_values')
      .where('expense_id', expense.id).orderBy('id', 'desc').first();
    const user = await Database.from('users').where('id', expense.user_id).first();
    return { ...expense, expenseValue, user };
  }

  static async queryExpenses({ ...params }) {
    const { user_id, all } = params;
    const { initialDate, finalDate } = this.getDateRange(params);
    let arr = [];

    const expenses = await Database.raw(
      `SELECT expenses.* FROM expenses
      WHERE expenses.created_at >= '${initialDate} 00:00:00'
      AND expenses.created_at <= '${finalDate} 23:59:59'
      ${(user_id && !all) ? `AND expenses.user_id = ${user_id}`: ` `}
      OR expenses.fixed_expense = true
      ORDER BY expenses.fixed_expense DESC, expenses.id DESC`
    );

    for(let ex in expenses.rows) {
      let expense = expenses.rows[ex];
      let user = await Database.from('users').where('id', '=', expense.user_id).first();
      let expenseValue = await Database.from('expense_values')
        .where('expense_id', '=', expense.id).orderBy('id', 'desc').first();
      arr.push({ ...expense, user, expenseValue });
    }
    return arr;
  }

  static getDateRange(params) {
    let { initialDate, finalDate } = params;
    if(!initialDate && !finalDate) {
      return { 
        initialDate: firstDayOfMonth(), 
        finalDate: format(lastDayOfMonth(new Date()), 'yyyy-MM-dd') 
      }
    } else {
      return {
        initialDate: format(addDays(new Date(initialDate), 1), 'yyyy-MM-dd'),
        finalDate: format(addDays(new Date(finalDate), 1), 'yyyy-MM-dd')
      }
    }
  }

  // static scopeQueryExpenses(query, { ...params }) {
  //   const { user_id, all } = params;
  //   const { initialDate, finalDate } = getDateRage();
  //   const sql = query.with('expenseValues')
  //                   .with('user')
  //                   .where('expenses.created_at', '>=', `${initialDate} 00:00:00`)
  //                   .where('expenses.created_at', '<=', `${finalDate} 00:00:00`)
  //                   .orWhere('expenses.fixed_expense', '=', true)
  //   if(user_id && !all) return sql.where('expenses.user_id', user_id);
  //   return sql.orderBy('expenses.fixed_expense','desc').orderBy('expenses.id','desc');
  // }
}

module.exports = Expense;
