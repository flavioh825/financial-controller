'use strict'

const { format } = require('date-fns');

const firstDayOfMonth = () => {
  return format(new Date(new Date().getFullYear(), new Date().getMonth(), 1), 'yyyy-MM-dd');
}

const currentDateTime = () => {
  let date = new Date();
  let current = new Date(date.valueOf() - date.getTimezoneOffset() * 60000);
  return current.toISOString().replace(/\.\d{3}Z$/, '');
} 

module.exports = { firstDayOfMonth, currentDateTime };